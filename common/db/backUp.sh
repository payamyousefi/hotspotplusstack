#!/bin/bash

# Create archive filename.
time=$(($(date +%s%N)/1000000))
m_now=`date +%Y%m%d`
hostname=$(hostname -s)
archive_file="$hostname-$m_now-$time.tgz"

# Where to backup to.
dest="/home/payam/backup/hotspotplus"

docker-compose stop mongodb
tar -cvf "$dest/hsp_mongo_$archive_file" ~/mongodb
docker-compose start mongodb

docker-compose stop elasticsearch
tar -cvf "$dest/hsp_elastic_$archive_file" ~/elasticsearch
docker-compose start elasticsearch