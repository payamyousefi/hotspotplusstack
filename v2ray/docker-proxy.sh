#!/bin/sh
mkdir /etc/systemd/system/docker.service.d
cp https-proxy.conf /etc/systemd/system/docker.service.d/
systemctl daemon-reload
systemctl show --property Environment docker
systemctl restart docker